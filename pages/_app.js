import React from 'react'
import Layout from "../components/common/Layout";
import {RecoilRoot} from 'recoil';
import "./../styles/global.scss"

const App = ({Component, pageProps}) => <RecoilRoot>
    <Layout>
        <Component {...pageProps} />
    </Layout>
</RecoilRoot>

export default App