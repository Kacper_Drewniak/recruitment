import Head from 'next/head'
import Layout from "../components/common/Layout";
import Todos from "../components/todos/Todos";
import {getTodos} from "../api/todos"
import React, {useEffect} from "react";
import {filteredTodoListState} from "../recoil/todos/Selectors"
import {useRecoilState} from 'recoil';

const Home = ({items}) => {
    const [todos, setTodos] = useRecoilState(filteredTodoListState)

    useEffect(() => {
        if (!todos.length) setTodos(items)
    }, [])

    return <Layout>
        <Head>
            <title>Task Recruitment</title>
        </Head>
        <Todos todos={todos}/>
    </Layout>
}


export default Home;

export async function getStaticProps() {
    const res = await getTodos()
    const items = (await res.json()).data
    return {
        props: {
            items,
        },
    }
}