import fetch from 'isomorphic-fetch'

export function getTodos () {
    return fetch('https://gorest.co.in/public-api/todos')
}

