import React from 'react';
import {todoListFilterState} from '../../recoil/todos/Atoms';
import {useRecoilState} from 'recoil';
import {Box} from 'rebass'
import {Label, Select} from '@rebass/forms'

const TodosListFilters = () => {
    const [filter, setFilter] = useRecoilState(todoListFilterState);
    const updateFilter = ({target: {value}}) => setFilter(value);
    const values = ['Show All', 'Show Completed', 'Show Uncompleted']

    return <Box flexBasis="15%">
        <Label>Choose Filter</Label>
        <Select onChange={updateFilter}
                defaultValue={filter}
                sx={{border:'none'}}
                backgroundColor="#03A9F4"
                value={filter}>
            {values.map(v => <option key={v}>{v}</option>)}
        </Select>
    </Box>
}

export default TodosListFilters