import React from 'react'
import {useRecoilState} from 'recoil'
import {Box, Button} from 'rebass'
import {useRouter} from 'next/router';
import {todoListState} from "../../recoil/todos/Atoms";
import TodoItem from "../../components/todos/TodosItem";

const Todo = () => {
    const router = useRouter();
    const [todosList, setTodosList] = useRecoilState(todoListState);
    const id = parseInt(router.query.id)
    const todo = todosList.find(x => x.id === id);
    const handleDelete = () => {
        const index = todosList.findIndex(x => x.id === id)
        setTodosList((oldItems) => {
            return oldItems.filter(x => x.id !== id)

        });
        router.push('/')
    }

    return <>
        <TodoItem item={todo || {}}/>
        <Box mx={'auto'} mt={3}>
            <Button backgroundColor={'#f3558e'} mr={2} onClick={handleDelete}>Delete</Button>
            <Button backgroundColor={"#6c757d"} onClick={() => router.push('/')}>Back</Button>
        </Box>
    </>

}

export default Todo;