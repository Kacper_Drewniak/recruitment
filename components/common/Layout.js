import {ThemeProvider} from 'emotion-theming'
import preset from '@rebass/preset'
import {Flex} from 'rebass'
import React from "react";

const flexProperties = {
    backgroundColor: "#CFD8DC",
    flexDirection: "column",
    alignItems: "center",
    width:"100%",
    minHeight:'100vh'
}

const Layout = ({children}) =>
    <ThemeProvider theme={preset}>
        <Flex {...flexProperties}
        >{children}</Flex>
    </ThemeProvider>


export default Layout
