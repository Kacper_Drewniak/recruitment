import React from 'react'
import {Button, Flex} from 'rebass'
import {useRouter} from "next/router";
import TodosListFilters from "./TodosListFilters";
import TodosSearch from "./TodosSearch";
import TodosVisibility from "./TodosVisibility";

const TodosOptions = () => {
    const router = useRouter()

    return <Flex  width="100%" alignItems="center" justifyContent="space-evenly">
        <Button backgroundColor="#03A9F4" alignSelf="flex-end" flexBasis="15%" onClick={() => router.push('/create')}>
            create todo
        </Button>
        <TodosListFilters/>
        <TodosSearch/>
        <TodosVisibility/>
    </Flex>
}


export default TodosOptions