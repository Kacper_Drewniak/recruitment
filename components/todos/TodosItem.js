import React, {useState} from 'react';
import {useRecoilState} from 'recoil';
import {todoListState} from '../../recoil/todos/Atoms';
import {Box, Button, Flex, Text} from 'rebass'
import {Label, Textarea} from '@rebass/forms'
import {useRouter} from "next/router";
import moment from 'moment'
import {charsCounter} from "./../../helpers/stringHelper"
import {pretifyMomentDate} from  "./../../helpers/dateHelper"
const replaceItemAtIndex = (arr, index, newValue) => [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];

const TodoItem = ({item}) => {
    const router = useRouter();
    const [todoList, setTodoList] = useRecoilState(todoListState);
    const [isEdit, setIsEdit] = useState(false)
    const [text, setText] = useState('')


    const handleEditable = () => {
        if (isEdit && text !== "") {
            const newList = replaceItemAtIndex(todoList, todoList.findIndex(x => x === item), {
                ...item,
                title: text,
            });
            setTodoList(newList);
        }
        setIsEdit(value => !value)
    }


    const handleChangeText = (event) => {
        const value = event.target.value;
        setText(value);
    }

    const toggleItemCompletion = () => {
        const index = todoList.findIndex(t => t.id === id)
        const newList = replaceItemAtIndex(todoList, index, {
            ...item,
            isComplete: !item.isComplete,
        });
        setTodoList(newList);
    };

    const {isComplete, id, title, created_at} = item;

    const todoProperties = {
        flexDirection: "column",
        justifyContent: "space-between",
        sx: {transition: ".3s", borderRadius: "5px",border:'2px solid lightgrey'},
        backgroundColor: isComplete ? "#c4a445" : "#ffe082",
        p: 2,
        m: 2,
        minHeight: "200px"
    }

    return <Box minWidth="300px" px={2} py={2} width={1 / 4}>
        <Flex {...todoProperties}>
            {isEdit ?
                <Textarea defaultValue={title} onChange={(e) => handleChangeText(e)}/>
                : <Text sx={{textDecoration: isComplete ? "line-through" : null}}>
                    {title}
                </Text>}
            <Label fontSize={[1, 1, 1]}><input
                type="checkbox"
                checked={isComplete}
                onChange={toggleItemCompletion}
            />{isComplete ? "Completed" : "Uncompleted"}</Label>
            {!isEdit &&  <Text fontSize={[1, 1, 1]}
                               color='grey'>Chars count: {charsCounter(title)}</Text>}
            <Text fontSize={[1, 1, 1]}
                  color='grey'>{pretifyMomentDate(created_at)}</Text>
            <Box> <Button style={{background: '#455A64'}} m={1}
                          onClick={() => router.push('/todos/[id]', `/todos/${id}`)}>Show</Button>
                <Button width="50%" alignSelf="flex-end" style={{background: isEdit ? "green" : 'yellowgreen'}}
                        m={1} onClick={() => handleEditable()}>{isEdit ? "Save" : "Edit"}</Button>
            </Box>
        </Flex>
    </Box>

};

export default TodoItem;