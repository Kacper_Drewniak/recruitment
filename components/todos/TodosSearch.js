import React from 'react';
import {useRecoilState} from 'recoil';
import {todoSearchState} from "../../recoil/todos/Atoms";
import {Input, Label} from '@rebass/forms';
import {Box} from 'rebass';

const TodosSearch = () => {
    const [search, setSearch] = useRecoilState(todoSearchState)
    const handleSearch = ({value}) => setSearch(value)

    return <Box flexBasis="15%">
        <Label htmlFor='search'>Search:</Label>
        <Input
            name='search'
            type='text'
            backgroundColor={'white'}
            placeholder='Search.....'
            value={search}
            onChange={(e) => handleSearch(e.target)}
        />
    </Box>

}


export default TodosSearch;