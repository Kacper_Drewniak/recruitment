import React from 'react';
import {useRecoilValue} from 'recoil';
import {todoListStatsState} from '../../recoil/todos/Selectors';
import {Box, Flex} from 'rebass'

const TodosListStats = () => {
    const {
        totalNum,
        totalCompletedNum,
        totalUncompletedNum,
    } = useRecoilValue(todoListStatsState);


    const stats = [{
        label: "number of todos",
        value: totalNum
    }, {
        label: "number of completed todos",
        value: totalCompletedNum
    }, {
        label: "number of uncompleted todos",
        value: totalUncompletedNum
    },
    ]

    return <Flex flexDirection="column"
                 justifyContent="space-between"
                 alignItems="space-around"
                 m={2}
                >
        {stats.map(s =>
            <Flex justifyContent="space-between"
                  backgroundColor="#00BCD4"
            m={1}>
                <Box
                    m={1}
                    p={1}
                    color='black'>
                    {s.label}
                </Box>
                <Box
                    p={1}
                    m={1}
                    fontWeight="bolder"
                    color='black'>
                    {s.value}
                </Box>
            </Flex>
        )}
    </Flex>
}

export default TodosListStats