import moment from 'moment'

export function toLocalDate(date) {
    return moment(date).local();
}

export function pretifyMomentDate(momentDate) {
    return moment(momentDate).format('LLLL');
}

export function toUtcDate(date) {
    return moment(date).UTC();
}