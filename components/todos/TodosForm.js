import React, {useState} from 'react';
import {useSetRecoilState} from 'recoil';
import { todoListState} from '../../recoil/todos/Atoms';
import {Button, Flex} from 'rebass'
import {Input} from '@rebass/forms'
import moment from 'moment';

let id = 0;

function getId() {
    return id++;
}

const TodosForm = () => {
    const [inputValue, setInputValue] = useState('');
    const setTodoList = useSetRecoilState(todoListState);

    const addItem = () => {
        setTodoList((oldTodoList) => [
            ...oldTodoList,
            {
                id: getId(),
                title: inputValue,
                isComplete: false,
                created_at: moment().format()
            },
        ]);
        setInputValue('')
    };

    const onChange = ({target: {value}}) => {
        setInputValue(value);
    };

    return (
        <Flex
            width="35%"
            m={5}>
            <Input type="text" value={inputValue} onChange={onChange} placeholder='create new todos'/>
            <Button onClick={addItem} sx={{
                ':hover': {
                    backgroundColor: 'tomato',
                    cursor: 'pointer'
                }
            }} mr={2}>+</Button>
        </Flex>
    );
}

export default TodosForm;