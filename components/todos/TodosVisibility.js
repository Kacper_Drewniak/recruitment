import React from 'react';
import {useRecoilState} from 'recoil';
import {todoListFilterState} from "../../recoil/todos/Atoms";
import {Button} from 'rebass';

const TodosVisibility = () => {
    const [filter, setFilter] = useRecoilState(todoListFilterState);

    const handleVisibility = () => {
        if (filter === 'Show All') setFilter('Show Uncompleted')
        else setFilter('Show All')
    }

    const buttonProperties = {
        backgroundColor: "#03A9F4",
        onClick: handleVisibility,
        flexBasis: "15%",
        alignSelf: "flex-end"
    }


    return <Button {...buttonProperties}>{filter === 'Show All' ? "Hide Completed" : "Show All"}</Button>

}

export default TodosVisibility