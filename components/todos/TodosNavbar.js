import React from 'react'
import {Flex, Heading} from 'rebass'
import TodosListStats from "./TodosListStats";

const TodosNavbar = () => <Flex
    alignItems="center"
    justifyContent="space-evenly"
    width="100%">
    <Heading
        m={2}
        fontSize={[5, 6, 7]}
        color='#212121'>
        Todo List
    </Heading>
    <TodosListStats/>
</Flex>


export default TodosNavbar