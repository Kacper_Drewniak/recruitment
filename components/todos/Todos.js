import {useRecoilValue, useSetRecoilState} from 'recoil';
import {todoListState, todoSearchState} from '../../recoil/todos/Atoms';
import {filteredTodoListState} from "../../recoil/todos/Selectors";
import React, {useEffect} from 'react';
import TodoItem from "./TodosItem";
import TodosNavbar from "./TodosNavbar";
import TodosOptions from "./TodosOptions";
import {Flex} from 'rebass';

const Todos = ({todos}) => {
    const setTodoList = useSetRecoilState(todoListState);
    const todoList = useRecoilValue(filteredTodoListState);
    const search = useRecoilValue(todoSearchState)
    const isSeach = search === ""
    const todoListResearch = todoList.filter(t => t.title.toLowerCase().includes(search.toLowerCase()))

    useEffect(() => setTodoList(todos), [])
    const printTodoList = todoList => todoList.map(t => <TodoItem item={t}/>)

    console.log(todoList)

    return <>
        <TodosNavbar/>
        <TodosOptions/>
        <Flex justifyContent="space-evenly" flexWrap='wrap' width="100%">
            {isSeach ? printTodoList(todoList) : printTodoList(todoListResearch)}
        </Flex>
    </>

}

export default Todos;




