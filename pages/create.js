import React, {useState} from 'react'
import {todoListState} from '../recoil/todos/Atoms'
import {useSetRecoilState} from 'recoil'
import {Input} from '@rebass/forms'
import {Button,Text} from "rebass";
import {useRouter} from 'next/router'
import moment from "moment";

let id = 200;

function getId() {
    return id++;
}

const Create = () => {
    const router = useRouter();
    const [inputValue, setInputValue] = useState('');
    const setTodoList = useSetRecoilState(todoListState);

    const handleSaveItem = () => {
        setTodoList((oldTodoList) => [
            ...oldTodoList,
            {
                id: getId(),
                title: inputValue,
                isComplete: false,
                created_at: moment().format()
            },
        ]);
        router.push('/')
    }

    const onChange = ({target: {value}}) => setInputValue(value);

    return <form>
            <Text fontSize={3}>Task:</Text>
            <Input minWidth={'300px'}
                   type='text'
                   placeholder='Task description'
                   onChange={(e) => onChange(e)}
                   backgroundColor={'white'}
                   mb={3}/>
                   <Button onClick={handleSaveItem}>Add</Button>
        </form>
}

export default Create